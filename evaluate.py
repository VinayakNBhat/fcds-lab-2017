import sys
import csv

EXPECTED_RUNS = 5

# proudly copy-pasted from https://stackoverflow.com/questions/24101524/finding-median-of-list-in-python
def median(lst):
    lst = sorted(lst)
    n = len(lst)
    if n < 1:
            return None
    if n % 2 == 1:
            return lst[n//2]
    else:
            return sum(lst[n//2-1:n//2+1])/2.0

def evaluate_file(fname):
    with open(fname, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        single = []
        octa = []
        runs = 0
        for row in reader:
            if len(row) != 5 and len(row) != 4:
                break
            name = row[0]
            if row[2].strip() == "1":
                runs += 1
                single.append(int(row[3]))
            if row[2].strip() == "8":
                octa.append(int(row[3]))

        if not len(single) == EXPECTED_RUNS or not len(octa) == EXPECTED_RUNS:
            print name, "unexpected number of runs"
        if len(single) == 0 or len(octa) == 0:
            print name, "not enough values for speedup calculation"
        else:
            med_single = median(single)
            med_octa = median(octa)
            spdup = float(med_single)/med_octa
            print name, "runs:", runs, "median 1t runtime:", med_single, "median 8t runtime:", med_octa, "speedup:", spdup

if __name__ == "__main__":
    for x in sys.argv[1:]:
        evaluate_file(x)
